package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

type Stock struct {
	Symbol string `json:"symbol"`
	Name string `json:"companyName"`
	Price float64 `json:"latestPrice"`
}

func main() {

	url := "https://cloud.iexapis.com/beta/stock/amd/quote?token=sk_aefa26f9ee6749349492c1591c05003a"
	var stock Stock

	stockClient := http.Client{
		Timeout: time.Second * 2,
	}

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Fatal(err)
	}

	res, getErr := stockClient.Do(req)
	if getErr != nil {
		log.Fatal(err)
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(err)
	}

	json.Unmarshal([]byte(body), &stock)


	fmt.Println(stock)

}
